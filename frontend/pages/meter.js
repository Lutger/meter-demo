import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { AppShell, Header, Text, SimpleGrid, Loader, Container, Space } from '@mantine/core';
import { DateRangePicker } from '@mantine/dates';

const MeterPage = () => {
    const [loading, setLoading] = useState(true);
    const [value, setValue] = useState({
        start: new Date(2019, 0, 1),
        end: new Date(2019, 1, 1),
    });
    const [data, setData] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            try {
                const query = { params: value };
                const url = 'http://localhost:5000/meter';
                const response = await axios.get(url, query);
                setData(response.data);
            } catch (error) {
                console.error(error.message);
            }
            setLoading(false);
        };

        fetchData();
    }, [value]);

    const handleDateRangeChange = (e) => {
        if (e[0] && e[1]) {
            setValue({ start: e[0], end: e[1] });
        }
    }

    const formatDate = (datestring) => {
        const date = new Date(datestring);
        return date.toLocaleDateString('nl') + ' ' + date.toLocaleTimeString('nl');
    }

    return (
        <AppShell
            padding="md"
            header={<Header p="md">{<Text size="lg" weight={500}>Meter data demo</Text>}</Header>}
        >
            <Container px="xs">
                <DateRangePicker
                    label="Select period"
                    placeholder="Pick two dates"
                    value={value}
                    clearable={false}
                    onChange={handleDateRangeChange}
                    defaultValue={[new Date(2019, 0, 1), new Date(2019, 0, 4)]}
                    minDate={new Date(2019, 0, 1)}
                    maxDate={new Date(2019, 0, 31)}
                    allowLevelChange={false}
                />
                <Space h="md" />

                {loading && <Loader />}
                {!loading && (
                    <SimpleGrid cols={2} spacing="xl">
                        {
                            data.flatMap(item => [<div key={item.timestamp}>{formatDate(item.timestamp)}</div>, <div key={item.timestamp + '_value'}>{item.value}</div>])
                        }
                    </SimpleGrid>
                )}
            </Container>
        </AppShell>

    )
}

export default MeterPage