from grpc import insecure_channel

from meter_pb2 import MeterRequest
from meter_pb2_grpc import MeterServiceStub
from flask import request, jsonify, Flask

app = Flask(__name__)

@app.route('/meter', methods=['GET'])
@app.route('/meter/<start>/<end>', methods=['GET'])
def meter():
    channel = insecure_channel('service:50051')
    stub = MeterServiceStub(channel)
    start = request.args.get('start')
    end = request.args.get('end')

    meter_request = MeterRequest(start=start, end=end)
    response = jsonify([{'timestamp': measurement.timestamp, 'value': measurement.value}
                       for measurement in stub.GetMeasurements(meter_request).measurements])
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
