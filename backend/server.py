import bisect
import csv
import datetime
from concurrent import futures
from datetime import datetime

import grpc
from dateutil.parser import isoparse

import meter_pb2
import meter_pb2_grpc
from meter_pb2 import Measurement, MeterResponse

def parse_datetime(datestr):
    return datetime.strptime(datestr, '%Y-%m-%d %H:%M:%S')

def get_meter_data_from_csv(path):
    with open(path, mode='r') as meter_csv:
        reader = csv.reader(meter_csv)
        next(reader)  # skip header
        values = [[parse_datetime(x), float(y)]
                  for x, y in reader if float(y) == float(y)]
        return sorted(values, key=lambda x: x[0])

class MeterRepository(object):
    ''' 
    Provides data access for meter data
    '''
    def __init__(self):
        self.__data = get_meter_data_from_csv('data/meterusage.1649328959.csv')

    def __get_datetime_index(self, datetime_value):
        if (type(datetime_value) != datetime):
            raise Exception(
                "type error: parameter must be a datetime object, not ", type(datetime_value))
        return bisect.bisect_left(self.__data, datetime_value, key=lambda x: x[0])

    def list(self, start, end):
        '''
        Takes a start and end datime object, returns a list of [timestamp, value] constituting the range of start:end
        '''
        start_index = 0 if start == None else self.__get_datetime_index(start)
        end_index = len(self.__data) - \
            1 if end == None else self.__get_datetime_index(end)
        return self.__data[start_index:end_index]


class MeterServiceServicer(meter_pb2_grpc.MeterServiceServicer):
    def __init__(self, repo):
        self.__repo = repo

    def GetMeasurements(self, request, context):
        parse_param = lambda x: None if not x else isoparse(x).replace(tzinfo=None)
        create_measurement = lambda x: Measurement(timestamp=x[0].isoformat(), value=x[1])

        measurement_list = self.__repo.list(parse_param(request.start), parse_param(request.end))
        measurements = [create_measurement(measurement) for measurement in measurement_list]
        return MeterResponse(measurements=measurements)

def serve():
    '''
    Start the grpc service
    '''
    print('start service')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    repository = MeterRepository()
    meter_pb2_grpc.add_MeterServiceServicer_to_server(
        MeterServiceServicer(repository), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    serve()
