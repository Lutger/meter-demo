# Demo metering app

This example app shows a simple single page app getting data from a json api, which queries a grpc service.

## Usage

Run the application with:

```
docker-compose up
```

Browse to `localhost:3000/meter` to see it in action.
